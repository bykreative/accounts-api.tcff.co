const Account = require("../models/account.model.js");

const NotificationUtil = require("./notification.util");

module.exports = {
    checkForUsername: (username) => {
        NotificationUtil.sendSlackMessage("2: checking for username: "+username, "CF3U0LPDF");
        return new Promise((resolve, error) => {
            Account.findOne({username: username}).exec((err, account) => {
                if (err) {
                    error(err);
                }
                else {
                    if (!account) {
                        resolve(false);
                    }
                    resolve(true);
                }
            });
        });

    },
    checkForEmail: (email) => {
        NotificationUtil.sendSlackMessage("3: checking for email: "+email, "CF3U0LPDF");
        return new Promise((resolve, error) => {
            Account.findOne({email: email}).exec((err, account) => {
                if (err) {
                    error(err);
                }
                else {
                    if (!account) {
                        resolve(false);
                    }
                    resolve(true);
                }
            })
        });
    },
    getUsernameWithEmail: (email) => {
        NotificationUtil.sendSlackMessage("2: getting a username from email: "+email, "CF3U0LPDF");
        return new Promise((resolve, error) => {
            Account.findOne({email: email}).exec((err, account) => {
                if (err) {
                    error(err);
                }
                else {
                    if (!account) {
                        resolve({code: "no_account"});
                    }
                    else {
                        resolve({code: "passed", username: account.username});
                    }
                }
            })
        });
    }
};