const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountSchema = new Schema({
    username: String,
    saltedPassword: String,
    email: String,
    phone: {type: String, unique: false},
    meta: {
      dateJoined: {
        day: Number,
        month: Number,
        year: Number
      },
      firstName: String,
      lastName: String,
        birthday: {
          day: Number,
          month: Number,
          year: Number
        }
    },
    profilePhotoUrl: String,
    verificationType: String,
    isVerified: Boolean
});

AccountSchema.index({username: 1, email: 1}, {unique: true});

module.exports = mongoose.model('Account', AccountSchema, 'accounts');
